// A number consisting entirely of ones is called a repunit. We shall define R(k) to be a
// repunit of length k.
// For example, R(10) = 1111111111 = 11×41×271×9091, and the sum of these prime
// factors is 9414.
// Write a function accepting repunit and return sum of prime factors

// Function (R(k)) {
// Return SUM(All Prime factors)
// }

const generatePrimeFactors = max => {
  const primeFactors = [];
  while (max % 2 === 0) {
    primeFactors.push(2);
    max = max / 2;
  }

  const sqrtNum = Math.sqrt(max);
  for (var i = 3; i <= sqrtNum; i++) {
    while (max % i === 0) {
      primeFactors.push(i);
      max = max / i;
    }
  }

  if (max > 2) {
    primeFactors.push(max);
  }
  return primeFactors;
};

const calculate = max => {
  let result = 0;
  const primes = generatePrimeFactors(max);
  for (let i = 0; i < primes.length; i++) {
    result += primes[i];
  }
  return result;
};

const sumValue = calculate(1111111111);
console.log('sumValue', sumValue);
